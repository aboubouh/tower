package com.nespresso.exercise.tower;

import java.util.LinkedList;

import com.nespresso.exercise.tower.floor.Floor;
import com.nespresso.exercise.tower.floor.FloorFactory;
import com.nespresso.exercise.tower.floor.FloorWithWindows;
import com.nespresso.exercise.tower.floor.NormalFloor;

public class Tower {

	private LinkedList<Floor> floors;

	public Tower() {
		floors = new LinkedList<>();
	}

	public void addFloor(int floorSize) {
		checkIfLastFloorSupportsAnother(floorSize);
		floors.add(FloorFactory.createNormalFloor(floorSize));
	}

	public void addFloorWithWindows(int floorSize, int desiredNumberOfWindows) {
		checkIfLastFloorSupportsAnother(floorSize);
		floors.add(FloorFactory.createFloorWithWindows(floorSize, desiredNumberOfWindows));
	}
	
	private void checkIfLastFloorSupportsAnother(int nextFloorSize){
		if (!floors.isEmpty() && !floors.getLast().supports(nextFloorSize)) {
			throw new IllegalArgumentException();
		}
	}

	public String printFloor(int floorIndex) {
		return floors.get(floorIndex).printFloor(sizeOfRamainedFloors(floorIndex));
	}

	private int sizeOfRamainedFloors(int floorIndex) {
		return floors.size() - floorIndex - 1;
	}

}
