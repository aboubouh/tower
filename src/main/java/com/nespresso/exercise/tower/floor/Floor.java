package com.nespresso.exercise.tower.floor;

public abstract class Floor {

	protected int size;

	public Floor(int size) {
		this.size = size;
	}

	public boolean supports(int nextFloorSize) {
		return this.size >= nextFloorSize;
	}

	protected String print(int size) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			sb.append("X");
		}
		return sb.toString();
	}

	public abstract String printFloor(int sizeOfRamainedFloors);

}
