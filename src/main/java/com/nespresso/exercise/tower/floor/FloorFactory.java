package com.nespresso.exercise.tower.floor;

public class FloorFactory {

	public static Floor createNormalFloor(int floorSize){
		return new NormalFloor(floorSize);
	}
	
	public static Floor createFloorWithWindows(int floorSize, int desiredNumberOfWindows){
		return new FloorWithWindows(floorSize, desiredNumberOfWindows);
	}
}
