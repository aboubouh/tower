package com.nespresso.exercise.tower.floor;

public class FloorWithWindows extends Floor {

	private int numberOfWindows;

	public FloorWithWindows(int size, int numberOfWindows) {
		super(size);
		this.numberOfWindows = numberOfWindows;
	}

	private String printWindow() {
		return "0";
	}

	private String printWindowsSeperated(int sizeOfWindows, int seperationRate) {
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i <= sizeOfWindows; i++) {
			if (sb.toString().length() < size - 2) {
				sb.append(printWindow());
			}
			if (i % seperationRate == 0 && sb.toString().length() < size - 2) {
				sb.append("X");
			}
		}
		return sb.toString();
	}

	private String printWindows(int sizeOfWindows, int sizeOfRamainedFloors) {
		StringBuilder sb = new StringBuilder();
		switch (sizeOfRamainedFloors) {
		case 0:
			for (int i = 0; i < sizeOfWindows; i++) {
				if (sb.toString().length() < size - 2)
					sb.append(printWindow());
			}
			break;
		case 1:
			sb.append(printWindowsSeperated(sizeOfWindows, 3));
			break;
		case 2:
			sb.append(printWindowsSeperated(sizeOfWindows, 2));
			break;
		default:
			sb.append(printWindowsSeperated(sizeOfWindows, 1));
		}
		return sb.toString();
	}

	@Override
	public String printFloor(int sizeOfRamainedFloors) {

		StringBuilder sb = new StringBuilder();
		int ramainingSizeCount = (this.size - 2 - printWindows(numberOfWindows, sizeOfRamainedFloors).length());
		int sizeOfSidesOfWindows = ramainingSizeCount / 2;

		sb.append(print(1));
		sb.append(print(sizeOfSidesOfWindows));
		sb.append(printWindows(numberOfWindows, sizeOfRamainedFloors));
		if (ramainingSizeCount % 2 == 0) {
			sb.append(print(sizeOfSidesOfWindows));
		} else {
			sb.append(print(sizeOfSidesOfWindows + 1));
		}
		sb.append(print(1));

		return sb.toString();
	}

}
